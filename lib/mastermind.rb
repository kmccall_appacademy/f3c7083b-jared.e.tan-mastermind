class Code
  attr_reader :pegs

  PEGS = {}

  def initialize(pegs = '')
    @pegs = pegs
  end

  def self.parse(string)
    unless string.chars.all? { |i| 'rgbyop'.include?(i.downcase) }
      raise 'Not a valid string'
    end
    unless string.length == 4
      raise 'Not a valid string'
    end
    self.new(string)
  end

  def self.random
    colors = ['r', 'g', 'b', 'y', 'o', 'p']
    answer = ''
    4.times { answer << colors[rand(6)] }
    self.new(answer)
  end

  def [](i)
    @pegs[i]
  end

  def exact_matches(guess)
    count = 0
    (0..3).each do |i|
      count += 1 if guess[i] == @pegs[i]
    end
    count
  end

  # let(:code1) { Code.parse("yyyb") }
  # let(:code2) { Code.parse("bboy") }

  def near_matches(guess)
    count = 0
    hash = Hash.new(0)
    answer = @pegs.dup
    (0..3).each { |i| hash[answer[i]] += 1 }
    (0..3).each do |j|
      if hash[guess[j]] > 0
        count += 1
        hash[guess[j]] -= 1
      end
    end
    count - exact_matches(guess)
  end

  def ==(other)
    return false unless other.class == Code
    self.pegs.downcase == other.pegs.downcase
  end
end



class Game
  attr_reader :secret_code, :turns

  def initialize(secret_code = Code.random, turns = 10)
    @secret_code = secret_code
    @turns = turns
    @guess = ''
  end

  def get_guess
    print "What is your guess? colors are: rgbyop | ex) 'rbgy': "
    gets.chomp.downcase
  end

  def display_matches(guess)
    # print 'Exact Matches'
    puts "Exact matches: #{@secret_code.exact_matches(guess)}"
    puts "Near matches: #{@secret_code.near_matches(guess)}"
  end

  def play
    until @secret_code.exact_matches(@guess) ==  4 || @turns < 0
      puts "#{turns} turns remaining"
      @guess = get_guess
      display_matches(@guess)
      @turns -= 1
    end
    result(@turns)
  end

  def result(turns)
    if turns < 0
      puts "Sorry, try again. The answer was #{@secret_code.pegs}"
    else
      puts 'Congrats, you did it!'
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
